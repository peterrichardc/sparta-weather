package com.labsidea.weather.ui.institution.detail

import android.content.Context
import com.labsidea.weather.R
import com.labsidea.weather.api.APIServiceInterface
import com.labsidea.weather.models.WeatherPrevisions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CityDetailPresenter(private val city_id_selected: Int): CityDetailContract.Presenter{
    private val api: APIServiceInterface = APIServiceInterface.create()

    private val subscriptions = CompositeDisposable()

    private lateinit var view: CityDetailContract.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }


    override fun attach(view: CityDetailContract.View, context: Context) {
        this.view = view

        val subscribe = api.getWeatherPrevisionByCityCityID(city_id_selected.toString(), context.getString(R.string.celcius_fahre),  context.getString(R.string.my_language_acronym))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe({ weatherPrevisions: WeatherPrevisions? ->


                    view.showProgress(false)

                    //Atualiza tela.
                    view.loadDataSuccess(weatherPrevisions)


                }, { error ->
                    view.showProgress(false)
                    if (error.message != null)
                        view.showErrorMessage(error.message!!)
                })

        subscriptions.add(subscribe)
    }

}