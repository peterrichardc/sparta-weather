package com.labsidea.weather.ui.institution

import android.content.Context
import com.labsidea.weather.R
import com.labsidea.weather.api.APIServiceInterface
import com.labsidea.weather.models.City
import com.labsidea.weather.models.db.MyCities
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.Sort
import java.lang.Exception

class CitiesPresenter : CitiesContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private val api: APIServiceInterface = APIServiceInterface.create()

    private lateinit var view: CitiesContract.View

    private var context: Context? = null

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: CitiesContract.View, context: Context) {
        this.view = view

        this.context = context
    }


    override fun updateDatabase(city: City?, is_favorite: Boolean) {
        try {
            if (city != null) {
                view.showProgress(true)
                val realm = Realm.getDefaultInstance()
                realm.executeTransaction {
                    val myCity = it.where(MyCities::class.java).equalTo("id", city.id).findFirst()

                    if (myCity != null) {
                        // myCity.id = city.id
                        myCity.name = city.name
                        myCity.is_favorite = is_favorite
                    }
                }
            }

        } catch (e: Exception) {
            view.showErrorMessage(e.message!!)
        }
    }


    //Salva a cidade adicionada
    override fun saveInDatabase(city: City?) {
        try {

            if (city != null) {
                view.showProgress(true)
                val realm = Realm.getDefaultInstance()
                //realm.beginTransaction()

                realm.executeTransaction {

                    val myCity = MyCities()
                    myCity.id = city.id
                    myCity.name = city.name
                    myCity.is_favorite = false

                    it.insertOrUpdate(myCity)
                }
            }
        } catch (e: Exception) {
            view.showErrorMessage(e.message!!)
        }
    }

    //Busca as cidades do usuario no banco de dados.
    override fun loadCities() {
        val realm = Realm.getDefaultInstance()
        //Ordena pelos favoritos
        val cities: List<MyCities> = realm.copyFromRealm(realm.where(MyCities::class.java).sort("is_favorite", Sort.DESCENDING).findAll())

        if (cities.isEmpty()) {
            view.showProgress(false)

            view.onWithoutDataToLoad()
        } else
            this.loadCitiesByList(cities)
    }

    private fun loadCitiesByList(cities: List<MyCities>) {

        view.showProgress(true)
        //Varre todas as cidades e busca por elas dentro do WebService.
        cities.forEachIndexed { index, myCity ->
            loadCities(myCity, index == cities.size - 1)
        }

    }

    override fun loadCities(myCity: MyCities, isLastCall: Boolean) {

        val subscribe = api.getCities(myCity.name, context!!.getString(R.string.celcius_fahre), context!!.getString(R.string.my_language_acronym))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe({ city: City? ->

                    //P ordenar pela lista de favoritos.
                    city?.is_favorite = myCity.is_favorite!!

                    if (isLastCall)
                        view.showProgress(false)

                    //Atualiza tela com cidade recebida.
                    view.loadDataSuccess(city)


                }, { error ->
                    view.showProgress(false)
                    view.showErrorMessage(error.message!!)
                })

        subscriptions.add(subscribe)
    }


    override fun deleteItem(item: MyCities) {
        //TODO DELETE MyCities
    }

}