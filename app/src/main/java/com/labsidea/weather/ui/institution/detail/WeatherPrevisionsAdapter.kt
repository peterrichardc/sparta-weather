package com.labsidea.weather.ui.institution.detail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.labsidea.weather.R
import com.labsidea.weather.models.WeatherPrevision
import com.labsidea.weather.models.WeatherPrevisions
import kotlinx.android.synthetic.main.item_city.view.*
import java.util.*

class WeatherPrevisionsAdapter(val list: List<WeatherPrevision?>): RecyclerView.Adapter<WeatherPrevisionsAdapter.WeatherPrevisionsViewHolder>() {


    inner class WeatherPrevisionsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){


        fun bind(weatherPrevision: WeatherPrevision?){
            itemView.tvAdd.visibility = View.GONE
            itemView.favorite.visibility = View.GONE

// convert seconds to milliseconds
            val date = java.util.Date(weatherPrevision!!.date.toLong() * 1000L)
// the format of your date
            val sdf = java.text.SimpleDateFormat("dd-MM-yyyy")
// give a timezone reference for formatting (see comment at the bottom)
            sdf.timeZone = java.util.TimeZone.getTimeZone("GMT-4")
            val formattedDate = sdf.format(date)

            itemView.tvName.text = formattedDate.toString()

            itemView.tvWeather.text = weatherPrevision?.weather?.first()?.description?.capitalize()

            var temperatureFormatted = "${weatherPrevision?.temperature?.temp_min?.toInt()}º - ${weatherPrevision?.temperature?.temp_max?.toInt()}º"
            itemView.tvTemperatures.text = temperatureFormatted

            temperatureFormatted = "${weatherPrevision?.temperature?.temp?.toInt()}º"
            itemView.tvActuallyTemperature.text = temperatureFormatted

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherPrevisionsViewHolder {
        val inflate: LayoutInflater = LayoutInflater.from(parent.context)
        val view: View = inflate.inflate(R.layout.item_city, parent, false)

        return WeatherPrevisionsViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeatherPrevisionsViewHolder, position: Int) {
        holder.bind(list[position])
    }


    override fun getItemCount() = list.size
}