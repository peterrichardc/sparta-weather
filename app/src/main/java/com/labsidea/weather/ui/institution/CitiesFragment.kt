package com.labsidea.weather.ui.institution

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.labsidea.weather.R
import com.labsidea.weather.di.component.DaggerFragmentComponent
import com.labsidea.weather.di.module.FragmentModule
import kotlinx.android.synthetic.main.fragment_cities.*
import javax.inject.Inject
import com.labsidea.weather.ui.institution.detail.CityDetailActivity
import org.jetbrains.anko.support.v4.startActivity
import android.support.v7.widget.SearchView
import android.view.*
import com.labsidea.weather.models.City
import com.labsidea.weather.models.db.MyCities
import org.jetbrains.anko.support.v4.toast


class CitiesFragment: Fragment(), CitiesContract.View, CitiesAdapter.CitiesAdapterEvents{


    @Inject lateinit var presenter: CitiesContract.Presenter

    private var lastQueryString = ""

    private lateinit var rootView: View

    companion object {
        val TAG: String = "CitiesFragment"
    }

    fun newInstance(): CitiesFragment {
        return CitiesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cities, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        injectDependency()

      //  val model = ViewModelProviders.of(this@CitiesFragment)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.attach(this, context!!)
        presenter.subscribe()

        presenter.loadCities()

        //To hear filter edit.
        svSearchCities.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {

                if (lastQueryString != query){
                    (rvCities?.adapter as CitiesAdapter?)?.list?.clear()
                    (rvCities?.adapter as CitiesAdapter?)?.notifyDataSetChanged()

                    if (query.isNotEmpty()){
                        val cityToQuery = MyCities()
                        cityToQuery.name = query

                        presenter.loadCities(cityToQuery, true)
                    }
                    else //Limpou o filtro de pesquisa
                        presenter.loadCities()

                }

                lastQueryString = query


                svSearchCities.clearFocus() // hide keyboard

                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                //Limpou o filtro de pesquisa
                if (newText.isEmpty()){
                    (rvCities?.adapter as CitiesAdapter?)?.list?.clear()
                    (rvCities?.adapter as CitiesAdapter?)?.notifyDataSetChanged()
                    presenter.loadCities()

                    svSearchCities.clearFocus() // hide keyboard
                }

                return true
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }

    override fun showProgress(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showErrorMessage(error: String) {
        Log.e("Error", error)
    }

    override fun loadDataSuccess(city: City?) {
        //val newHeight = main_container.height - (btnFilter.height + 10)
        //val behavior = BottomSheetBehavior.from(rvCities)
        //behavior.setPeekHeight(newHeight)
        //behavior.isHideable = false
        //behavior.setState(BottomSheetBehavior.STATE_COLLAPSED)

        if (rvCities?.adapter == null){
            //Cria o adaptador para o recyclerView.
            //Adiciona a primeira cidade da lista.
            val list = mutableListOf<City?>()
            list.add(city)
            val adapter = CitiesAdapter(context!!, list, this)

            rvCities?.layoutManager = LinearLayoutManager(context)
            rvCities?.adapter = adapter

            layoutWithoutCities?.visibility = View.GONE
            rvCities?.visibility = View.VISIBLE
        }
        else{
            //Adiciona outra cidade na lista para o recyclerView.
            val adapter = rvCities?.adapter as CitiesAdapter?

            if (adapter?.list != null){
                val element = adapter.list.indexOf(city)
                if (element != -1) //Existe altera o registro existente.
                    adapter.list[element] = city
                else
                    adapter.list.add(city) //Insere o novo registro
            }

            else
                adapter?.list?.add(city)

            //Garantir ordenacao pelos favoritos.
            adapter?.list?.sortByDescending { it?.is_favorite }

            adapter?.notifyDataSetChanged()
        }

    }

    override fun onClickItem(city: City?) {
        if (city != null)
            startActivity<CityDetailActivity>("city_id_selected" to city.id)
    }


    override fun onAddCity(city: City?) {
        //Grava a cidade selecionada no banco de dados.
        presenter.saveInDatabase(city)

        svSearchCities.setQuery("", false)

        toast(R.string.sucess_added)
    }

    override fun onFavoriteCity(city: City?, is_favorite: Boolean){
        //Grava a cidade selecionada no banco de dados.
        presenter.updateDatabase(city, is_favorite)

        showProgress(false)
    }

    private fun injectDependency() {
        val listComponent = DaggerFragmentComponent.builder()
                .fragmentModule(FragmentModule())
                .build()

        listComponent.inject(this)
    }

    override fun onWithoutDataToLoad() {
        layoutWithoutCities?.visibility = View.VISIBLE
        rvCities?.visibility = View.GONE

    }




}