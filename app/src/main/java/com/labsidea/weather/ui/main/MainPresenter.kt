package com.labsidea.weather.ui.main

import android.content.Context
import io.reactivex.disposables.CompositeDisposable

class MainPresenter: MainContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var view: MainContract.View

    override fun subscribe() {

    }

    override fun unsubscribe() {
        subscriptions.clear()
    }


    override fun attach(view: MainContract.View, context: Context) {
        this.view = view

        view.showCitiesFragment() // as default

    }
}