package com.labsidea.weather.ui.institution.detail

import com.labsidea.weather.models.WeatherPrevisions
import com.labsidea.weather.ui.base.BaseContract

interface CityDetailContract{

    interface View: BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun loadDataSuccess(weatherPrevisions: WeatherPrevisions?)
    }

    interface Presenter: BaseContract.Presenter<View>
}