package com.labsidea.weather.ui.institution

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.labsidea.weather.api.APIServiceInterface
import com.labsidea.weather.models.City
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CitiesViewModel : ViewModel() {

    val city: LiveData<City> = loadCities("Blumenau")

    private val api: APIServiceInterface = APIServiceInterface.create()

    private fun loadCities(cityName: String = ""): LiveData<City> {
        val city = MutableLiveData<City>()

        api.getCitiesCallback(cityName).enqueue(object : Callback<City> {

            override fun onResponse(call: Call<City>, response: Response<City>) {

                city.value = response.body()
            }

            override fun onFailure(call: Call<City>, t: Throwable) {
                //
            }
        })

        return city
    }
}