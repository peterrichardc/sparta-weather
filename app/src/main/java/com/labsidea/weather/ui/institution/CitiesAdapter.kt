package com.labsidea.weather.ui.institution

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.labsidea.weather.R
import com.labsidea.weather.models.City
import com.labsidea.weather.models.db.MyCities
import kotlinx.android.synthetic.main.item_city.view.*
import com.neovisionaries.i18n.CountryCode
import io.realm.Realm


class CitiesAdapter(val context: Context, val list: MutableList<City?>, val events: CitiesAdapterEvents): RecyclerView.Adapter<CitiesAdapter.CitiesAdapterViewHolder>() {

    interface CitiesAdapterEvents{
        fun onClickItem(city: City?)

        fun onAddCity(city: City?)
        fun onFavoriteCity(city: City?, is_favorite: Boolean)

    }

    inner class CitiesAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){


        fun bind(city: City?){
            itemView.tvName.text = city?.name

            val cc = CountryCode.getByCode(city?.country?.country)

            itemView.tvCountry.text = cc.getName()
            itemView.tvWeather.text = city?.weathers?.first()?.description?.capitalize()

            var temperatureFormatted = "${city?.temperature?.temp_min?.toInt()}º - ${city?.temperature?.temp_max?.toInt()}º"
            itemView.tvTemperatures.text = temperatureFormatted

            temperatureFormatted = "${city?.temperature?.temp?.toInt()}º"
            itemView.tvActuallyTemperature.text = temperatureFormatted



            val result = Realm.getDefaultInstance().where(MyCities::class.java).equalTo("id", city?.id).findFirst()
            var cityObjectTable: MyCities? = null
            if (result != null)
                cityObjectTable = Realm.getDefaultInstance().copyFromRealm(result)

            //Verifica se a cidade ja esta adicionada, se ja estiver adicionada, nao faz sentido o botao "Adicionar" aparecer.
            itemView.tvAdd.visibility = if (cityObjectTable != null) View.GONE else View.VISIBLE

            //Verifica se a cidade ja esta adicionada, se ja estiver adicionada, mostra o botao de favoritar, se nao faz sentido o botao "Favoritar" aparecer.
            itemView.favorite.visibility = if (cityObjectTable != null) View.VISIBLE else View.GONE

            //Favoritar
            itemView.favorite.setOnClickListener {
                if (cityObjectTable != null){
                    cityObjectTable.is_favorite = !cityObjectTable.is_favorite
                    events.onFavoriteCity(city, cityObjectTable.is_favorite)

                    itemView.favorite.isFavorite = cityObjectTable.is_favorite
                }

            }

            itemView.favorite.isFavorite = cityObjectTable?.is_favorite ?: false



            itemView.tvAdd.setOnClickListener {
                events.onAddCity(city)
            }
            itemView.main_container.setOnClickListener {
                //Checagem pq pode ser que seja clicado quando esta procurando uma cidade para adicionar.
                //So visualizar a tela de detalhes quando a cidade ja estiver adicionada a sua lista de cidades.
                //if (cityObjectTable != null)
                    events.onClickItem(city)
            }


        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesAdapterViewHolder {
        val inflate: LayoutInflater = LayoutInflater.from(parent.context);
        val view: View = inflate.inflate(R.layout.item_city, parent, false)

        return CitiesAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CitiesAdapterViewHolder, position: Int) {
        holder.bind(list[position])
    }


    override fun getItemCount() = list.size
}