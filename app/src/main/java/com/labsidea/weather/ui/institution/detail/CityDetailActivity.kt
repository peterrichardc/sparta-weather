package com.labsidea.weather.ui.institution.detail

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.labsidea.weather.R
import com.labsidea.weather.di.component.DaggerActivityComponent
import com.labsidea.weather.di.module.ActivityModule
import com.labsidea.weather.models.WeatherPrevisions
import com.neovisionaries.i18n.CountryCode
import kotlinx.android.synthetic.main.activity_city_detail.*
import javax.inject.Inject

class CityDetailActivity : AppCompatActivity(), CityDetailContract.View {


    @Inject
    lateinit var presenter: CityDetailContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_city_detail)

        toolbar.setNavigationIcon(android.R.drawable.ic_menu_close_clear_cancel)
        toolbar.setNavigationOnClickListener { finish() }

   //     loadAdapter(Clients().allActived())

        toolbar.setTitleTextColor(Color.WHITE)
        toolbar.setSubtitleTextColor(Color.WHITE)

        injectDependency()

        presenter.attach(this, this)

        //presenter.loadData()


    }


    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
                .activityModule(ActivityModule(this))
                .build()

        activityComponent.inject(this)
    }

    override fun showProgress(show: Boolean) {

    }

    override fun showErrorMessage(error: String) {

    }

    override fun loadDataSuccess(weatherPrevisions: WeatherPrevisions?) {

        if (weatherPrevisions?.list != null) {

            toolbar.title = weatherPrevisions.city?.name
            if (weatherPrevisions.city?.country != null){
                val cc = CountryCode.getByCode(weatherPrevisions.city!!.country!!.country)
                toolbar.subtitle = cc.getName()
            }



            rvPrevisions.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rvPrevisions.adapter = WeatherPrevisionsAdapter(weatherPrevisions.list!!)
        }

    }


}