package com.labsidea.weather.ui.base

import android.content.Context

interface BaseContract{

    interface Presenter<in T> {
        fun subscribe()
        fun unsubscribe()
        fun attach(view: T, context: Context)
    }

    interface View

}