package com.labsidea.weather.ui.main

import com.labsidea.weather.ui.base.BaseContract

class MainContract {

    interface View: BaseContract.View {
        fun showCitiesFragment()
    }

    interface Presenter: BaseContract.Presenter<MainContract.View> {
        //fun onClick
    }
}