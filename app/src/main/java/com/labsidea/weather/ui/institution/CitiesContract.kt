package com.labsidea.weather.ui.institution

import com.labsidea.weather.models.City
import com.labsidea.weather.models.db.MyCities
import com.labsidea.weather.ui.base.BaseContract

interface CitiesContract{

    interface View: BaseContract.View {
        fun showProgress(show: Boolean)
        fun showErrorMessage(error: String)
        fun onWithoutDataToLoad()
        fun loadDataSuccess(city: City?)
    }

    interface Presenter: BaseContract.Presenter<View> {
        fun loadCities()
        fun loadCities(myCity: MyCities, isLastCall: Boolean)
        fun saveInDatabase(city: City?)
        fun updateDatabase(city: City?, is_favorite: Boolean)
        fun deleteItem(item: MyCities)
    }
}