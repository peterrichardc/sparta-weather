package com.labsidea.weather.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.labsidea.weather.R
import com.labsidea.weather.di.component.DaggerActivityComponent
import com.labsidea.weather.di.module.ActivityModule
import com.labsidea.weather.ui.institution.CitiesFragment
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainContract.View {

    @Inject lateinit var presenter: MainContract.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        injectDependency()

        presenter.attach(this, this)

    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
                .activityModule(ActivityModule(this))
                .build()

        activityComponent.inject(this)
    }


    override fun showCitiesFragment() {

        supportFragmentManager.beginTransaction()
                .disallowAddToBackStack()
                //.setCustomAnimations(AnimType.SLIDE.getAnimPair().first, AnimType.SLIDE.getAnimPair().second)
                .replace(R.id.frmMain, CitiesFragment().newInstance(), CitiesFragment.TAG)
                .commit()

    }


}
