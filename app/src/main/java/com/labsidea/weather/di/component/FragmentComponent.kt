package com.labsidea.weather.di.component

import com.labsidea.weather.di.module.FragmentModule
import com.labsidea.weather.ui.institution.CitiesFragment
import dagger.Component

@Component(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {

    fun inject(listFragment: CitiesFragment)

}