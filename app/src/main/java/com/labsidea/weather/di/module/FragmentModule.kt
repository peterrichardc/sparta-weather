package com.labsidea.weather.di.module

import com.labsidea.weather.ui.institution.CitiesContract
import com.labsidea.weather.ui.institution.CitiesPresenter
import dagger.Module
import dagger.Provides

@Module
class FragmentModule {

    /*
    @Provides
    fun provideAboutPresenter(): AboutContract.Presenter {
        return AboutPresenter()
    }*/

    @Provides
    fun provideInstitutionPresenter(): CitiesContract.Presenter {
        return CitiesPresenter()
    }


}