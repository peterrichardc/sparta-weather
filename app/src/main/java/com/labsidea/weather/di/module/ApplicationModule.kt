package com.labsidea.weather.di.module

import android.app.Application
import com.labsidea.weather.BaseApp
import com.labsidea.weather.di.scope.PerApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val baseApp: BaseApp) {

    @Provides
    @Singleton
    @PerApplication
    fun provideApplication(): Application {
        return baseApp
    }
}