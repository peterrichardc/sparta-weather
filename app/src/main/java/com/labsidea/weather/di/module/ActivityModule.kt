package com.labsidea.weather.di.module

import android.app.Activity
import com.labsidea.weather.ui.institution.detail.CityDetailContract
import com.labsidea.weather.ui.institution.detail.CityDetailPresenter
import com.labsidea.weather.ui.main.MainContract
import com.labsidea.weather.ui.main.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private var activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun providePresenter(): MainContract.Presenter {
        return MainPresenter()
    }

    @Provides
    fun provideCityDetailActivity(): CityDetailContract.Presenter {
        return CityDetailPresenter(activity.intent.getIntExtra("city_id_selected", 0))
    }




}