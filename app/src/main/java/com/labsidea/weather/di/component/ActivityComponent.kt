package com.labsidea.weather.di.component

import com.labsidea.weather.ui.main.MainActivity
import com.labsidea.weather.di.module.ActivityModule
import com.labsidea.weather.ui.institution.detail.CityDetailActivity
import dagger.Component

@Component(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(activity: MainActivity)

    fun inject(activity: CityDetailActivity)

}