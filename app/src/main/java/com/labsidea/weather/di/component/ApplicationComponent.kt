package com.labsidea.weather.di.component

import com.labsidea.weather.BaseApp
import com.labsidea.weather.di.module.ApplicationModule
import dagger.Component

@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(application: BaseApp)

}