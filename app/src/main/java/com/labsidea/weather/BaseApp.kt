package com.labsidea.weather

import android.app.Application

import com.labsidea.weather.di.component.ApplicationComponent
import com.labsidea.weather.di.component.DaggerApplicationComponent
import com.labsidea.weather.di.module.ApplicationModule
import io.realm.Realm

class BaseApp: Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        instance = this
        setup()

        if (BuildConfig.DEBUG) {
            // Maybe TimberPlant etc.
        }

        Realm.init(this)

        /*Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        //.enableWebKitInspector(Realm RealmInspectorModulesProvider.builder(this).build())
                        .build());*/
    }

    fun setup() {
        component = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance: BaseApp private set
    }
}