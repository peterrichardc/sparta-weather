package com.labsidea.weather.api

import com.labsidea.weather.BuildConfig
import com.labsidea.weather.models.*
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.http.Query


interface APIServiceInterface{

    /*
    For temperature in Fahrenheit use units=imperial
    For temperature in Celsius use units=metric
    Temperature in Kelvin is used by default, no need to use units parameter in API call
     */
    @GET("weather")
    fun getCities(@Query("q") cityName: String,
                  @Query("units") units: String = "metric",
                  @Query("lang") language: String = "pt",
                  @Query("APPID") idKey: String = "5b50ca7a85adbc22f6e533796ac791cf"): Observable<City>

    @GET("forecast")
    fun getWeatherPrevisionByCityCityID(@Query("id") id: String,
                  @Query("units") units: String = "metric",
                  @Query("lang") language: String = "pt",
                  @Query("APPID") idKey: String = "5b50ca7a85adbc22f6e533796ac791cf",
                  @Query("cnt") cnt: String = "5"): Observable<WeatherPrevisions>



    @GET("weather")
    fun getCitiesCallback(@Query("q") cityName: String,
                  @Query("APPID") idKey: String = "5b50ca7a85adbc22f6e533796ac791cf",
                  @Query("lang") language: String = "pt",
                  @Query("units") units: String = "metric"): Call<City>

    companion object Factory {
        fun create(): APIServiceInterface {
            val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(rxAdapter)//RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BuildConfig.BASE_URL)
                    .build()

            return retrofit.create(APIServiceInterface::class.java)
        }
    }
}