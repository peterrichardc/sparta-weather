package com.labsidea.weather.models

open class Temperature {

    var temp: Double = 0.0
    var temp_min: Double = 0.0
    var temp_max: Double = 0.0
}