package com.labsidea.weather.models

import com.google.gson.annotations.SerializedName
open class WeatherPrevisions {

    var city: City? = null

    var list: List<WeatherPrevision>? = null
}