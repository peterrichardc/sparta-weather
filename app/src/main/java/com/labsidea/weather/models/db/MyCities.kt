package com.labsidea.weather.models.db

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class MyCities: RealmObject(){
    @PrimaryKey
    var id: Int = 0
    var name: String = ""
    var is_favorite: Boolean = false


}