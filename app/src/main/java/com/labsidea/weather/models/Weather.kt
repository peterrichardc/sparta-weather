package com.labsidea.weather.models

open class Weather {

    var id: Int = 0
    var main: String = ""
    var description: String = ""
}