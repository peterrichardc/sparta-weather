package com.labsidea.weather.models

import com.google.gson.annotations.SerializedName



class CitiesList{

    @SerializedName("institutions")
    val cities: List<City>? = null
}