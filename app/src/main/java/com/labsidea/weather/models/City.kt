package com.labsidea.weather.models

import com.google.gson.annotations.SerializedName
open class City {

    var id: Int = 0
    var name: String = ""

    var cod: Int = 0

    @SerializedName("sys")
    var country: Country? = null

    @SerializedName("weather")
    var weathers: List<Weather?>? = null

    @SerializedName("main")
    var temperature: Temperature? = null

    var is_favorite: Boolean = false


}