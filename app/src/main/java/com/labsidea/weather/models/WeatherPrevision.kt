package com.labsidea.weather.models

import com.google.gson.annotations.SerializedName
open class WeatherPrevision {

    @SerializedName("dt")
    var date: String = ""

    @SerializedName("main")
    var temperature: Temperature? = null

    var weather: List<Weather?>? = null

}