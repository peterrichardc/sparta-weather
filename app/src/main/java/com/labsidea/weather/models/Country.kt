package com.labsidea.weather.models

open class Country {
    var id: Int = 0
    var country: String = ""
}